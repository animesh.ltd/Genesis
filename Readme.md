# Genesis

I am building a computer from scratch.

## ALU design
Accepts two 16-bit inputs `x` and `y`, and six control bits and returns a 16-bit `output`. The control bits
are as follows (in order of their precedence):
- `zx` zero x-input
- `zy` zero y-input
- `nx` invert x-input
- `ny` invert y-input
- `f` function selector. `0` for `x AND y`, `1` for `x + y`.
- `no` invert output

Additionally, the ALU computes two 1-bit outputs:
- `zr`: indicates whether the ouput is zero. `1` if `output = 0`, `0` otherwise.
- `ng`: indicates whether the output is negative. `1` if `output < 0`, `0` otherwise.

Theoretically, six control bits allow for an ALU that can perform 2<sup>6</sup> = 64 functions. The current design
supports 18 functions only:
- `x + y`
- `x - y`
- `y - x`
- `0`: Set output as `0` irrespective of input
- `1`: Set output as `1` irrespective of input
- `-1`
- `x`
- `y`
- `-x`
- `-y`
- `!x`
- `!y`
- `x + 1`
- `y + 1`
- `x - 1`
- `y - 1`
- `x & y`: bitwise AND
- `x | y`: bitwise OR