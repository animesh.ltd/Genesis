//
//  Xor.tst
//  Genesis/Chips
//
//  Created on 09 September 2018 by Animesh Mishra <hello@animesh.ltd>
//  © 2018 Animesh Mishra. All Rights Reserved.
//

load Xor.hdl,
output-file Xor.out,
compare-to Xor.cmp,
output-list a%B3.1.3 b%B3.1.3 out%B3.1.3;

set a 0,
set b 0,
eval,
output;

set a 0,
set b 1,
eval,
output;

set a 1,
set b 0,
eval,
output;

set a 1,
set b 1,
eval,
output;