//
//  Inc16.tst
//  Genesis/Chips
//
//  Created on 27 September 2018 by Animesh Mishra <hello@animesh.ltd>
//  © 2018 Animesh Mishra. All Rights Reserved.
//

load Inc16.hdl,
output-file Inc16.out,
compare-to Inc16.cmp,
output-list in%B1.16.1 out%B1.16.1;

set in %B0000000000000000,  // in = 0
eval,
output;

set in %B1111111111111111,  // in = -1
eval,
output;

set in %B0000000000000101,  // in = 5
eval,
output;

set in %B1111111111111011,  // in = -5
eval,
output;