//
//  Not.tst
//  Genesis/Chips
//
//  Created on 09 September 2018 by Animesh Mishra <hello@animesh.ltd>
//  © 2018 Animesh Mishra. All Rights Reserved.
//

load Not.hdl,
output-file Not.out,
compare-to Not.cmp,
output-list in%B3.1.3 out%B3.1.3;

set in 0,
eval,
output;

set in 1,
eval,
output;