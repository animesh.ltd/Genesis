/**
    Bit.hdl
    Genesis/Chips

    Created on 13 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

/**
    A single-bit register, or simply Bit, is designed to store a single
    bit of information (0 or 1).

    Inputs:     in, load
    Outputs:    out
    Behaviour:
        If load(t-1) then out(t) = in(t-1)
        else out(t) = out(t-1)
**/
CHIP Bit {
    IN in, load;
    OUT out;

    PARTS:
    Mux(a=feedback, b=in, sel=load, out=input);
    DFF(in=input, out=feedback, out=out);
}